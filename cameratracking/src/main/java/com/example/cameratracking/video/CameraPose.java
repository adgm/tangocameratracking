package com.example.cameratracking.video;

import android.content.Context;

import com.example.cameratracking.calibration.PreferencesCommon;
import com.google.atap.tangoservice.TangoPoseData;
import com.serenegiant.tango.PositionRecorder;

public class CameraPose {
    public static float[] getCalculatedOrientation(Context context, float[] orientation){
        float[] orientationChange = PreferencesCommon.getAllFromPreferences(context);

        for (int i = 0; i < 3 ; i++){
            orientation[i] += orientationChange[i];
        }

        return orientation;
    }

    public static void passCameraPose(Context context, TangoPoseData pose){
        float[] translation = pose.getTranslationAsFloats();
        float[] rotation = getCalculatedOrientation(context, pose.getRotationAsFloats());
        PositionRecorder.setActualPosition(translation, rotation);
    }
}
