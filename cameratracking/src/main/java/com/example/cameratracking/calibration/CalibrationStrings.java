package com.example.cameratracking.calibration;

public class CalibrationStrings {
    public static final String PREFERENCES_NAME = "calibrationPreferences";
    public static final String PREFERENCES_X_ROTATION = "xRotation";
    public static final String PREFERENCES_Y_ROTATION = "yRotation";
    public static final String PREFERENCES_Z_ROTATION = "zRotation";
    public static final String PATTERN_NOT_NUMBER = "[^0-9\\.]+";
    public static final String PATTERN_LEFT = "lewo";
    public static final String PATTERN_RIGHT = "prawo";
    public static final String PATTERN_UP = "gór";
    public static final String PATTERN_DOWN = "dół";
    public static final String PATTERN_RADIANS = "radian";
    public static final String PATTERN_DEGREES = "stop";
}
