package com.example.cameratracking.calibration;



import static com.example.cameratracking.calibration.CalibrationStrings.PATTERN_DEGREES;
import static com.example.cameratracking.calibration.CalibrationStrings.PATTERN_LEFT;
import static com.example.cameratracking.calibration.CalibrationStrings.PATTERN_NOT_NUMBER;
import static com.example.cameratracking.calibration.CalibrationStrings.PATTERN_UP;

public class MessageAnalyzer {

    public static boolean isUnitDegrees(String msg){
        return msg.toLowerCase().contains(PATTERN_DEGREES);
    }

    public static float returnRotation(String msg){
        float rotation = getRotation(msg);
        if(isOppositeDirection(msg))
            rotation = changeDirection(rotation);
        if(isUnitDegrees(msg))
            rotation = changeUnit(rotation);

        return rotation;
    }

    public static float getRotation(String msg){
        msg = msg.replaceAll(PATTERN_NOT_NUMBER,"");
        if(msg != "")
        {
            return Float.parseFloat(msg);
        }
        return 0.0f;
    }

    public static boolean isOppositeDirection(String voiceMessage){
        return (voiceMessage.toLowerCase().contains(PATTERN_LEFT)
                || voiceMessage.toLowerCase().contains(PATTERN_UP));
    }

    public static float changeDirection(float rotation){
        return rotation * (-1);
    }

    public static float changeUnit(float rotation){
        return (float) Math.toRadians(rotation);
    }

    public static String returnInfo(String msg){
        float rotation = getRotation(msg);
        if(isOppositeDirection(msg))
            rotation = changeDirection(rotation);
        if(isUnitDegrees(msg))
        {
            return "Odczytana wartość: " + String.format("%s", rotation) + "°";
        }
        else
            return "Odczytana wartość: " + String.format("%s", rotation) + " rad";
    }
}
