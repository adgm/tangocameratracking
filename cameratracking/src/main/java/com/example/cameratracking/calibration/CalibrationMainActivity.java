package com.example.cameratracking.calibration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.cameratracking.R;

public class CalibrationMainActivity extends AppCompatActivity {
    private TextView valuesTextView;
    private Button buttonTextCalibration;
    private Button buttonVoiceCalibration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration_main);

        restoreData();

        buttonTextCalibration = (Button) findViewById(R.id.buttonTextCalibration);
        buttonTextCalibration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalibrationMainActivity.this,
                        TextCalibrationActivity.class);
                startActivity(intent);
            }
        });

        buttonVoiceCalibration = (Button) findViewById(R.id.buttonVoiceCalibration);
        buttonVoiceCalibration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalibrationMainActivity.this,
                        VoiceXActivity.class);
                startActivity(intent);
            }
        });

    }

    private void restoreData(){
        float[] rotationDifference;
        rotationDifference = PreferencesCommon.getAllFromPreferences(this);
        valuesTextView = (TextView) findViewById(R.id.currSettingsText);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("x = " + rotationDifference[0] + " rad\n\ny = " + rotationDifference[1] +
                " rad\n\nz = " + rotationDifference[2] + " rad");

        valuesTextView = (TextView) findViewById(R.id.currSettingsText);
        valuesTextView.setText(stringBuilder.toString());
    }
}
