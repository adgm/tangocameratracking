package com.example.cameratracking.calibration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.cameratracking.R;

public class TextCalibrationActivity extends AppCompatActivity {
    float[] rotationDifference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_calibration);

        final Spinner unitSpinner = (Spinner) findViewById(R.id.unitSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.units_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        unitSpinner.setAdapter(adapter);

        Button okButton = (Button) findViewById(R.id.tcButton);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getValues();
                checkUnit(unitSpinner.getSelectedItem().toString());
                PreferencesCommon.saveToPreferences(TextCalibrationActivity.this,
                        rotationDifference);

                Intent intent = new Intent(TextCalibrationActivity.this,
                        CalibrationMainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getValues(){
        final EditText xText = (EditText) findViewById(R.id.xEditText);
        final EditText yText = (EditText) findViewById(R.id.yEditText);
        final EditText zText = (EditText) findViewById(R.id.zEditText);
        rotationDifference = new float[3];
        rotationDifference[0] = Float.valueOf(xText.getText().toString());
        rotationDifference[1] = Float.valueOf(yText.getText().toString());
        rotationDifference[2] = Float.valueOf(zText.getText().toString());
    }

    private void checkUnit(String selectedUnit){
        if (MessageAnalyzer.isUnitDegrees(selectedUnit)){
            convertToRadians();
        }
    }

    private void convertToRadians(){
        for(int i = 0; i < 3; i++)
        {
            rotationDifference[i] = (float)Math.toRadians(rotationDifference[i]);
        }
    }
}
