package com.example.cameratracking.calibration;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import static com.example.cameratracking.calibration.CalibrationStrings.PREFERENCES_NAME;
import static com.example.cameratracking.calibration.CalibrationStrings.PREFERENCES_X_ROTATION;
import static com.example.cameratracking.calibration.CalibrationStrings.PREFERENCES_Y_ROTATION;
import static com.example.cameratracking.calibration.CalibrationStrings.PREFERENCES_Z_ROTATION;

public class PreferencesCommon {

    public static SharedPreferences getPreferences(Context context){
        return context.getSharedPreferences(PREFERENCES_NAME, Activity.MODE_PRIVATE);
    }
    public static void saveToPreferences(Context context, String rotationName, float rotationValue){
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putFloat(rotationName, rotationValue);
        editor.commit();
    }

    public static void saveToPreferences(Context context, float[] rotationDifference){
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putFloat(PREFERENCES_X_ROTATION, rotationDifference[0]);
        editor.putFloat(PREFERENCES_Y_ROTATION, rotationDifference[1]);
        editor.putFloat(PREFERENCES_Z_ROTATION, rotationDifference[2]);
        editor.commit();
    }

    public static float[] getAllFromPreferences(Context context){
        SharedPreferences preferences = getPreferences(context);
        float[] rotationDifference = new float[3];
        rotationDifference[0] = preferences.getFloat(PREFERENCES_X_ROTATION, 0.0f);
        rotationDifference[1] = preferences.getFloat(PREFERENCES_Y_ROTATION, 0.0f);
        rotationDifference[2] = preferences.getFloat(PREFERENCES_Z_ROTATION, 0.0f);
        return rotationDifference;
    }
}
