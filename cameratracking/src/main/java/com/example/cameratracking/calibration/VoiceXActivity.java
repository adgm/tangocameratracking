package com.example.cameratracking.calibration;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cameratracking.R;

import java.util.ArrayList;
import java.util.Locale;

import static com.example.cameratracking.calibration.CalibrationStrings.PREFERENCES_X_ROTATION;

public class VoiceXActivity extends AppCompatActivity {
    private TextView textView;
    private ImageButton micButton;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private String voiceMessage;
    private Button nextButton;
    private float rotation = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_x);

        micButton = (ImageButton) findViewById(R.id.xMicButton);
        textView = (TextView) findViewById(R.id.xHelpText);
        micButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptSpeechInput();
            }
        });

        nextButton = (Button) findViewById(R.id.xNextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferencesCommon.saveToPreferences(VoiceXActivity.this, PREFERENCES_X_ROTATION,
                        rotation);
                Intent intent = new Intent(VoiceXActivity.this, VoiceYActivity.class);
                startActivity(intent);
            }
        });
    }

    private void promptSpeechInput(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        }
        catch (ActivityNotFoundException a){
            Toast.makeText(getApplicationContext(), getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    voiceMessage = result.get(0);

                      rotation = MessageAnalyzer.returnRotation(voiceMessage);
                      textView.setText(MessageAnalyzer.returnInfo(voiceMessage));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
