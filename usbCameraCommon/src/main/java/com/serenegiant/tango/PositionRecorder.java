package com.serenegiant.tango;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PositionRecorder {
    public static float[] currentTranslation;
    public static float[] currentOrientation;
    public static List<String> positionList;
    public static File fileDir;
    public static String fileName;


    public static void setActualPosition(float[] translation, float[] orientation){
        currentTranslation = translation;
        currentOrientation = orientation;
    }

    private static String getPositionString(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Position: " +
                currentTranslation[0] + ", " + currentTranslation[1] + ", " + currentTranslation[2]);

        stringBuilder.append("     Orientation: " +
                currentOrientation[0] + ", " + currentOrientation[1] + ", " +
                currentOrientation[2] + ", " + currentOrientation[3]);

        return stringBuilder.toString();
    }

    public static String posStringkeep(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Position: " +
                currentTranslation[0] + ", " + currentTranslation[1] + ", " + currentTranslation[2]);

        stringBuilder.append("     Orientation: " +
                currentOrientation[0] + ", " + currentOrientation[1] + ", " +
                currentOrientation[2] + ", " + currentOrientation[3]);

        return stringBuilder.toString();
    }

    public static void addCurrentPositionToList(){
        positionList.add(getPositionString());
    }

    public static void savePositionListToFile(){
        try {
            File positionFile = new File(fileDir, fileName + ".txt");
            FileWriter writer = new FileWriter(positionFile);
            for (String position : positionList) {
                writer.append(position);
                writer.append(System.getProperty("line.separator"));
            }
            writer.flush();
            writer.close();
        }
        catch (IOException ex) {

        }

        positionList.clear();
    }



    public static void setFileDir(File dir){
        fileDir = dir;
    }

    public static void setFileName(String name){
        fileName = name;
    }

    public static void initList(){
        positionList = new ArrayList<String>();
    }
}
